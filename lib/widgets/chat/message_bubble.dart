import 'package:flutter/material.dart';

class MessageBubble extends StatelessWidget {
  final String messageText;
  final bool isMe;
  final String userName;
  final String userImage;
  const MessageBubble({
    Key? key,
    required this.messageText,
    required this.isMe,
    required this.userName,
    required this.userImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: isMe
                ? Colors.grey[300]
                : Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(12),
              topRight: const Radius.circular(12),
              bottomLeft: isMe ? const Radius.circular(12) : Radius.zero,
              bottomRight: isMe ? Radius.zero : const Radius.circular(12),
            ),
          ),
          width: 180,
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
          margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userName,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: isMe
                          ? Theme.of(context).colorScheme.secondary
                          : Colors.white,
                    ),
                  ),
                  Text(
                    messageText,
                    style: TextStyle(
                      color: isMe
                          ? Theme.of(context).colorScheme.secondary
                          : Colors.white,
                    ),
                  ),
                ],
              ),
              Positioned(
                top: -25,
                right: -30,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    CircleAvatar( radius: 20, backgroundImage: NetworkImage(userImage),),
                  ],
                ),
              ),
            ],
            clipBehavior: Clip.none,
          ),
        ),
      ],
    );
  }
}
