import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../chat/message_bubble.dart';

class Messages extends StatelessWidget {
  const Messages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentUser = FirebaseAuth.instance.currentUser;
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chat')
          .orderBy('createdAt', descending: true)
          .snapshots(),
      builder: (
        BuildContext ctx,
        AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> chatSnapShot,
      ) {
        if (chatSnapShot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        final chatDocs = chatSnapShot.data!.docs;

        return ListView.builder(
          reverse: true,
          itemCount: chatDocs.length,
          itemBuilder: (ctx, i) => MessageBubble(
            key: ValueKey(chatDocs[i].id),
            messageText: chatDocs[i]['text'],
            isMe: chatDocs[i]['userId'] == currentUser!.uid,
            userName: chatDocs[i]['userName'],
            userImage: chatDocs[i]['userImage'],
          ),
        );
      },
    );
  }
}
