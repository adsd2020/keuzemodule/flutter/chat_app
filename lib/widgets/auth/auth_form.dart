import 'dart:io';

import 'package:flutter/material.dart';

import './profile_picture.dart';

class AuthForm extends StatefulWidget {
  final void Function(
    String email,
    String username,
    String password,
    File? userImage,
    bool isLogin,
    BuildContext context,
  ) submitForm;
  final bool isLoading;
  const AuthForm({Key? key, required this.submitForm, required this.isLoading})
      : super(key: key);

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  var _isLogin = true;
  String? _userEmail;
  String? _userName;
  String? _password;
  File? _userImage;

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();

    if (!_isLogin && _userImage == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Please choose an image.',
            style: TextStyle(
              color: Theme.of(context).colorScheme.error,
            ),
          ),
          backgroundColor: Colors.black87,
        ),
      );
      return;
    }

    if (isValid && (_isLogin || _userImage != null)) {
      _formKey.currentState!.save();
      widget.submitForm(
        _userEmail as String,
        _userName ?? '',
        _password as String,
        _userImage,
        _isLogin,
        context,
      );
    }
  }

  void _iamgePicker(File? pickedImage) {
    _userImage = pickedImage;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (!_isLogin) ProfilePicture(imagePicker: _iamgePicker),
                  TextFormField(
                    key: const ValueKey('Email'),
                    autocorrect: false,
                    enableSuggestions: false,
                    validator: (value) {
                      if ((value == null || value.isEmpty) &&
                          !value!.contains('@')) {
                        return 'Please enter a valis emailaddress';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _userEmail = value;
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration:
                        const InputDecoration(labelText: 'Email address'),
                  ),
                  if (!_isLogin)
                    TextFormField(
                      key: const ValueKey('Username'),
                      autocorrect: true,
                      enableSuggestions: true,
                      textCapitalization: TextCapitalization.words,
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            value.length < 4) {
                          return 'Please enter a valid username';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _userName = value;
                      },
                      decoration: const InputDecoration(labelText: 'Username'),
                    ),
                  TextFormField(
                    key: const ValueKey('Password'),
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length < 6) {
                        return 'Please enter a valid passwoord';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _password = value;
                    },
                    obscureText: true,
                    decoration: const InputDecoration(labelText: 'Passsword'),
                  ),
                  const SizedBox(height: 12),
                  if (widget.isLoading)
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(),
                    ),
                  if (!widget.isLoading)
                    ElevatedButton(
                      onPressed: _trySubmit,
                      child: Text(_isLogin ? 'Login' : 'Signup'),
                    ),
                  if (!widget.isLoading)
                    TextButton(
                      onPressed: () {
                        setState(() {
                          _isLogin = !_isLogin;
                        });
                      },
                      child: Text(_isLogin
                          ? 'create new account'
                          : 'I already have an account'),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
